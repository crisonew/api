//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;


var requestjson=require('request-json');
var path = require('path');
var urlClientesMongo = "https://api.mlab.com/api/1/databases/cocampo/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientesMongo);

var urlInmueblesMongo="https://api.mlab.com/api/1/databases/cocampo/collections/Inmuebles?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var inmuebleMLab = requestjson.createClient(urlInmueblesMongo);

var urlUsuariosMongo="https://api.mlab.com/api/1/databases/cocampo/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMLab = requestjson.createClient(urlUsuariosMongo);

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/cocampo/collections";
var apiKey="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Acess-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.listen(port);
var movimientosJSONv2 = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res) {
  //res.send("Hola mundo desde node.js")
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/', function(req,res) {
  res.send("We've received your POST request");
})

app.put('/',function(req,res) {
  res.send("SOY UN PUT")

})

app.delete('/', function(req,res) {
  res.sendFile(path.join(__dirname,'delete.html'));
})

app.get('/Clientes',function(req,res) {
  res.send("Aquì estàn los clientes")
})

app.get('/Clientes/:idCliente',function(req,res) {
  res.send("Aquì tiene al cliente:" + req.params.idCliente)
})

app.get('/v1/Movimientos',function(req,res) {
  res.sendfile('movimientosv1.json')
})

app.get('/v2/Movimientos',function(req,res) {
  res.json(movimientosJSONv2)
})

app.get('/v2/Movimientos/:id',function(req,res) {
  console.log("El valor de la variable es " + req.params.id);
  res.send(movimientosJSONv2[req.params.id-1])
})

app.get('/v2/Movimientos/:id/:nombre',function(req,res) {
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
})

//Query
app.get('/v2/Movimientosq',function(req,res) {
  console.log("El valor de la variable es" + req.query);
  res.send(req.query)
})

app.get('/v2/Clientes',function(req,res) {
  clienteMLab.get('',function(err, resM,body) {
    console.log("The server has changes");
    if (err) {
      console.log(body);
    }
    else {
      res.send(body);
    }
  })
})

//inserta cliente en MongoDB a travès de MLab
app.post('/v2/Clientes', function(req,rest) {
  clienteMLab.post('', req.body, function(err, resM, body) {
    if (err) {
      console.log(err);
    }else {
      rest.send(body);
    }
  })
})

app.post('/v2/Usuarios', function(req,rest) {
  usuarioMLab.post('', req.body, function(err, resM, body) {
    if (err) {
      console.log(err);
    }else {
      rest.send(body);
    }
  })
})


app.get('/v2/Inmuebles',function(req,res) {
  inmuebleMLab.get('',function(err, resM,body) {
    console.log("The server has changes");
    if (err) {
      console.log(body);
    }
    else {
      res.send(body);
    }
  })
})

app.post('/v2/Inmuebles', function(req,rest) {
  inmuebleMLab.post('', req.body, function(err, resM, body) {
    if (err) {
      console.log(err);
    }else {
      rest.send(body);
    }
  })
})




//Peticion REST para login
app.post('/v2/Login',function(req,res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Acess-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var email = req.headers.email;
  var password = req.headers.password;
  var query ='q={"email":"'+email+'", "password":"' +password+ '"}';
  console.log(query);
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz+"/Usuarios?"+ apiKey + "&" + query);
  console.log(clienteMLabRaiz);

  clienteMLabRaiz.get('',function(err,resM,body) {

    if (!err) {
      if (body.length>0) {
        console.log("Log in");
        res.status(200).send("Usuario logado bien y adecuado");
      }
    }else {
      res.status(400).send("Usuario no encontrado");
    }
  })

})


app.get('/v2/Inmuebles',function(req,res) {
  inmuebleMLab.get('',function(err, resM,body) {
    if (err) {
      console.log(body);
    }
    else {
      console.log(body);
      res.send(body);
    }
  })
})

app.post('/v2/Inmuebles', function(req,rest) {
  clienteMLab.post('', req.body, function(err, resM, body) {
    if (err) {
      console.log(err);
    }else {
      rest.send(body);
    }
  })
})
